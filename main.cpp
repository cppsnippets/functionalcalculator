#include <unordered_map>
#include <iostream>
#include <functional>
#include <exception>

template <typename T>
class Calculator{
public:
    using eventNotifyCallback_t = std::function<T(T,T)>;

    Calculator(){
        this->operations['+'] = [this](T a, T b)->T{return this->add(a,b);};
        this->operations['-'] = [this](T a, T b)->T{return this->subtract(a,b);};
        this->operations['*'] = [this](T a, T b)->T{return this->multiply(a,b);};
        this->operations['/'] = [this](T a, T b)->T{return this->divide(a,b);};
    };
    ~Calculator();

    T execute(char op, T a, T b){
        T retVal;
        try{
            retVal = this->operations[op](a, b);
        }
        catch (std::exception e) {
            std::cout << "Operation is not supported. Only + - * / are supported. Details: " <<e.what() <<std::endl;
            std::cout << "Enter Num1 Operation Num2" << std::endl << "Example: 3 * 2" <<std::endl;
        }
        return retVal;
    }

private:
    T add(T a, T b) { return a + b; }
    T subtract(T a, T b) { return a - b; }
    T multiply(T a, T b) { return a * b; }
    T divide(T a, T b) { return (b == 0) ? 0 : a / b; }

    std::unordered_map<char, eventNotifyCallback_t> operations;
};

int main() {

    Calculator<int> *myCalculator = new Calculator<int>();
    std::cout << "Enter Num1 Operation Num2" << std::endl << "Example: 3 * 2" <<std::endl;
    // read the input
    char op;
    while(op != '.')
    {
        int num1, num2;

        std::cin >> num1 >> op >> num2;

        // perform the operation, as follows
        std::cout << "=> " <<  myCalculator->execute(op,num1,num2) << std::endl;
    }

}